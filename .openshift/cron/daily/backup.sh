#!/bin/bash

#Determine where to put the backup files
BACKUPDIR=$OPENSHIFT_DATA_DIR/backups
BACKUPFILE=$BACKUPDIR/$OPENSHIFT_APP_NAME.$(date -d "today" +"%Y%m%d-%H%M%S").sql
echo "Staring backup for $BACKUPFILE"

#Make the backup dir for the files, only if it exists
mkdir -p $BACKUPDIR

#Backup the DB to a file
mysqldump -h $OPENSHIFT_MYSQL_DB_HOST -P ${OPENSHIFT_MYSQL_DB_PORT:-3306} -u ${OPENSHIFT_MYSQL_DB_USERNAME:-'admin'} --password="$OPENSHIFT_MYSQL_DB_PASSWORD" --databases $OPENSHIFT_APP_NAME  > $BACKUPFILE
