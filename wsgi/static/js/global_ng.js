//Global Factory/Service to handle common behavior
myApp.factory('httpInterceptor', [function() {
    var httpInterceptor = {
        request: function(config) {
            config.timeout = 60000;
            config.xsrfHeaderName = 'X-CSRFToken';
            config.xsrfCookieName = 'csrftoken'
            return config;
        },
        response: function(response) {
            return response;
        },
        responseError: function(response) {
            if (response.status === 404 || response.status === 401 || response.status === 403) {
                window.location = '/';
            }
            return response;
        }
    };
    return httpInterceptor;
}]);

myApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);
