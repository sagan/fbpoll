function LoginCtrl($scope, $http) {

    $scope.loginUser = function() {
        //Remove any prior error messages
        $scope.result_txt = 'Checking Inputs...'

        if ($scope.username == "") {
            $scope.result_txt = "Email cannot be blank";
            return false;
        }

        if ($scope.password == "") {
            $scope.result_txt = "Password cannot be blank";
            return false;
        }

        //Lets do it
        $scope.result_txt = 'Sending Request...'

        //Send the inputs to the server
        var params = {'ts': new Date().getTime()};
        var inputData = {'_s_username':$scope.username, '_s_password':$scope.password};

        $http({
            method : 'POST',
            params: params,
            data: inputData,
            url : '/user_login/'
        })
        .success(function(data, status, headers, config) {
            $scope.result_txt = JSON.stringify(data);
        })
        .error(function(data, status, headers, config) {
            $scope.result_txt = JSON.stringify(data);
        });

    }; //end of loginUser()


    $scope.getCurrentUser = function() {
        //Remove any prior error messages
        $scope.result_txt = 'Getting User...'

        //Lets do it
        $scope.result_txt = 'Sending Request...'

        //Send the inputs to the server
        var params = {'ts': new Date().getTime()};
        params._s_foo = 'bar';

        $http({
            method : 'GET',
            params: params,
            url : '/user/'
        })
        .success(function(data, status, headers, config) {
            $scope.result_txt = JSON.stringify(data);
        })
        .error(function(data, status, headers, config) {
            $scope.result_txt = JSON.stringify(data);
        });

    }; //end of loginUser()


    //Initialize my view
    $scope.startDateTime = new Date();
    $scope.username = '';
    $scope.password = '';
    $scope.result_txt = ''
    $scope.getCurrentUser()

}

//Angular Module, used for defining dependencies
var myApp = angular.module('LoginApp', []);
myApp.controller('LoginCtrl', LoginCtrl);
