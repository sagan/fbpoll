function LoginFbCtrl($scope, $http) {

    $scope.loginFbUser = function() {
        //Remove any prior error messages
        $scope.fb_result_txt = 'Checking Inputs...'

        //Lets do it
        $scope.fb_result_txt = 'Sending Request...'

        //Send the inputs to the server
        var params = {'ts': new Date().getTime()};
        var inputData = {'fb_user_details':$scope.fb_user_details, 'fb_auth_response':$scope.fb_auth_response};

        $http({
            method : 'POST',
            params: params,
            data: inputData,
            url : '/user_login_fb/'
        })
        .success(function(data, status, headers, config) {
            $scope.fb_result_txt = JSON.stringify(data);
        })
        .error(function(data, status, headers, config) {
            $scope.fb_result_txt = JSON.stringify(data);
        });

    }; //end of loginFbUser()


    $scope.getCurrentUserFromWebSession = function() {
        //Remove any prior error messages
        $scope.server_result_txt = 'Getting User...'

        //Lets do it
        $scope.server_result_txt = 'Sending Request...'

        //Send the inputs to the server
        var params = {'ts': new Date().getTime()};
        params._s_foo = 'bar';

        $http({
            method : 'GET',
            params: params,
            url : '/user/'
        })
        .success(function(data, status, headers, config) {
            $scope.server_result_txt = JSON.stringify(data);
        })
        .error(function(data, status, headers, config) {
            $scope.server_result_txt = JSON.stringify(data);
        });

    }; //end of getCurrentUserFromWebSession()


    //Check with FB to see the user is logged in or not
    // This function is called when someone finishes with the Login Button.
    $scope.checkLoginState = function() {
      FB.getLoginStatus(function(response) {
        //Use APPLY, due to FB async callback
        $scope.$apply(function () {
            $scope.statusChangeCallback(response);
        });
      });
    }


    // This is called to process the results from FB.getLoginStatus()
    $scope.statusChangeCallback = function(response) {
      console.log('statusChangeCallback');
      console.log(response);
      $scope.fb_auth_response = response.authResponse;
      $scope.fb_user_details = null;
      // The response object is returned with a status field that lets the app know the current login status of the person.
      // Full docs on the response object can be found in the documentation for FB.getLoginStatus().
      if (response.status === 'connected') {
        // Logged into your app and Facebook.
        $scope.fb_login_status = "FULL";
        console.log('Welcome!  Fetching your information.... ');
        $scope.getUserDetails();
      } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        console.log('You are not logged into the App');
        $scope.fb_login_status = "PARTIAL";
        $scope.fb_login_msg = "You must Login to the App";
      } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        console.log('You are not logged into FB');
        $scope.fb_login_status = "NONE";
        $scope.fb_login_msg = "You must Login to FB";
      }
    }


    //Get the Logged in users details
    $scope.getUserDetails = function() {
        FB.api('/me', function(response) {
          console.log('Successful login for: ' + response.name);

          //Send to server
          $scope.fb_user_details = response;
          $scope.loginFbUser()

          //Use APPLY, due to FB async callback
            $scope.$apply(function () {
                $scope.fb_login_msg = 'Thanks for logging in, ' + response.name + '!';
            });
        });
    }


    // This is like logging out of the App, but not FB
    $scope.revokePermissions = function() {
      console.log('Revoking your permissions.... ');
      FB.api('/me/permissions', 'delete', function(response) {
        console.log('Successful Revoke');
      });
    }


    //Here is where the code starts executing automatically
    //Start the FB API
    //https://developers.facebook.com/docs/facebook-login/login-flow-for-web/v2.2
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '403690913118360',
        xfbml      : true,
        version    : 'v2.2'
      });

      //After the API is completely loaded, lets check the FB user login status
      $scope.checkLoginState();
    };


    // Load the SDK asynchronously
    (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));


    //Initialize my view
    $scope.startDateTime = new Date();
    $scope.server_result_txt = ''
    $scope.getCurrentUserFromWebSession()
    $scope.fb_login_status = 'UNKNOWN';
    $scope.fb_login_msg = 'UNKNOWN';
    $scope.fb_result_txt = 'UNKNOWN';
    $scope.fb_auth_response = null;
    $scope.fb_user_details = null;
}

//Angular Module, used for defining dependencies
var myApp = angular.module('LoginFbApp', []);
myApp.controller('LoginFbCtrl', LoginFbCtrl);
