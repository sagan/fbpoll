from django.db import models

# Create your models here.
from datetime import datetime


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    is_ready = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.now())
    modified_date = models.DateTimeField(auto_now=True, default=datetime.now())

    #The model_to_dict() cannot handle all types, so this takes care of it
    def to_json_dict(self):
        return dict(
            choices=[],
            pub_date=self.pub_date.isoformat(),
            question_text=self.question_text,
            id=self.id
    )

    def __str__(self):
        return 'Question: %s, %s, %s' % (self.id, self.created_date, self.modified_date)


class Choice(models.Model):
    question = models.ForeignKey(Question, related_name='choices')
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.now())
    modified_date = models.DateTimeField(auto_now=True, default=datetime.now())

    def to_json_dict(self):
        return dict(
            question=None,
            votes=self.votes,
            choice_text=self.choice_text,
            id=self.id
    )

    def __str__(self):
        return 'Choice: %s, %s, %s' % (self.id, self.created_date, self.modified_date)
