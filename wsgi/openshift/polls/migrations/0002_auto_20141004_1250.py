# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='choice',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2014, 10, 4, 12, 50, 20, 582713), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='choice',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2014, 10, 4, 12, 50, 20, 582713), auto_now=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2014, 10, 4, 12, 50, 20, 582713), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='is_ready',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='modified_date',
            field=models.DateTimeField(default=datetime.datetime(2014, 10, 4, 12, 50, 20, 582713), auto_now=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='choice',
            name='question',
            field=models.ForeignKey(to='polls.Question', related_name='choices'),
        ),
        migrations.AlterField(
            model_name='question',
            name='pub_date',
            field=models.DateTimeField(verbose_name='date published'),
        ),
    ]
