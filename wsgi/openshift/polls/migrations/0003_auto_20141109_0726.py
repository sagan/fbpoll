# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20141004_1250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choice',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2014, 11, 9, 7, 26, 34, 268243)),
        ),
        migrations.AlterField(
            model_name='choice',
            name='modified_date',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2014, 11, 9, 7, 26, 34, 268243)),
        ),
        migrations.AlterField(
            model_name='question',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, default=datetime.datetime(2014, 11, 9, 7, 26, 34, 268243)),
        ),
        migrations.AlterField(
            model_name='question',
            name='modified_date',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2014, 11, 9, 7, 26, 34, 268243)),
        ),
    ]
