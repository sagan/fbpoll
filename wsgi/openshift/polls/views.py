from cmath import log
from django.http import HttpResponse
from polls.models import Question, Choice
from django.utils import timezone
from django.db import transaction
import json
import logging
from django.contrib.auth.decorators import login_required

# Get an instance of a logger
logger = logging.getLogger(__name__)

#Add a new Question/Choice to the DB
@transaction.atomic
def hello(request):
    q = Question(question_text="What's new?", pub_date=timezone.now())
    q.save()

    c1 = Choice(choice_text="Not much", votes=9, question=q)
    c2 = Choice(choice_text="Nothing", votes=3, question=q)
    c1.save()
    c2.save()

    #How to access a specific child collection
    #q.choices.values()[0]

    #Convert the model objects to json ready
    dictQ = q.to_json_dict()
    dictC1 = c1.to_json_dict()
    dictQ['choices'].append(dictC1)
    dictC2 = c2.to_json_dict()
    dictQ['choices'].append(dictC2)

    #Respond
    data = json.dumps(dictQ)
    return HttpResponse(data, content_type="application/json")


#Get all the objects out of the DB
@login_required
def bye(request):
    logger.info("Calling the BYE UI")

    questions = Question.objects.all()
    #list(questions.values())
    listQs = []

    #Get inner join objects, for each question
    for question in questions:
        #This will assignment will perform an UPDATE to the CHOICE row; no need to do that
        #question.choices = Choice.objects.filter(question_id=question.pk)
        questionChoices = Choice.objects.filter(question_id=question.pk)

        #Convert to json
        dictQ = question.to_json_dict()
        listQs.append(dictQ)

        for choice in questionChoices:
            dictC = choice.to_json_dict()
            dictQ['choices'].append(dictC)

    #Respond
    data = json.dumps(listQs)
    return HttpResponse(data, content_type="application/json")
