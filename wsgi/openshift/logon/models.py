from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.

class UserFB(models.Model):
    created_date = models.DateTimeField(auto_now_add=True, default=datetime.now())
    modified_date = models.DateTimeField(auto_now=True, default=datetime.now())
    fb_user_id = models.CharField(max_length=50, unique=True)
    user = models.OneToOneField(User, unique=True)

    #The model_to_dict() cannot handle all types, so this takes care of it
    def to_json_dict(self):
        return dict(
            created_date=self.created_date.isoformat(),
            modified_date=self.modified_date.isoformat(),
            fb_user_id=self.fb_user_id,
            id=self.id,
            user=None
    )

    def __str__(self):
        return 'UserFB: %s, %s, %s, %s, %s' % (self.id, self.fb_user_id, self.created_date, self.modified_date, self.user.id)
