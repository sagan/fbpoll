# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserFB',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('created_date', models.DateTimeField(default=datetime.datetime(2014, 11, 15, 8, 2, 6, 269054), auto_now_add=True)),
                ('modified_date', models.DateTimeField(default=datetime.datetime(2014, 11, 15, 8, 2, 6, 269054), auto_now=True)),
                ('fb_user_id', models.CharField(max_length=50, unique=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
