from django.http import HttpResponse
import json
import logging
import logon.utility as logonUtility
import urllib
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from logon.models import UserFB


#Much of this logic and APi came from here
#https://docs.djangoproject.com/en/1.7/topics/auth/default/

# Get an instance of a logger
logger = logging.getLogger(__name__)


def isUserLoggedIn(request):
    #ts = request.GET['ts']
    if request.user.is_authenticated():
        data = {'result':True, 'username':request.user.username}
    else:
        data = {'result':False}

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def doLogin(request):
    logger.info(logonUtility.formatLogMessage(request, "Login Attempt"))
    #first logout any current users
    logout(request)

    #Reset the cookies to the default future datetime for both session and csrf cookies
    request.session.set_expiry(None)
    request.session.clear_expired()

    #JSON is in BODY payload as RAW not URL-Form-Encoded
    #username = request.POST['_s_email']
    #password = request.POST['_s_password']

    body = json.loads(request.body.decode("utf-8"))
    username = body['_s_username']
    password = body['_s_password']

    #Do the actual AUTH, this will set the csrf cookie
    user = authenticate(username=username, password=password)

    #Determine if user info was valid
    if user is not None:
        if user.is_active:
            login(request, user)
            data = {'result':True, 'username':user.username}
            logger.info(logonUtility.formatLogMessage(request, "Login Successful"))
        else:
            data = {'result':False}
            logger.info(logonUtility.formatLogMessage(request, "Login Failed, user is inactive"))
    else:
        data = {'result':False}
        logger.info(logonUtility.formatLogMessage(request, "Login Failed"))

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def doLoginFB(request):
    logger.info(logonUtility.formatLogMessage(request, "FB Login Attempt"))
    #first logout any current users
    logout(request)

    #Reset the cookies to the default future datetime for both session and csrf cookies
    request.session.set_expiry(None)
    request.session.clear_expired()

    body = json.loads(request.body.decode("utf-8"))
    fb_access_token = body['fb_auth_response']['accessToken']
    fb_user_id = body['fb_user_details']['id']

    try:
        #https://docs.djangoproject.com/en/dev/topics/db/transactions/#django.db.transaction.atomic
        with transaction.atomic():
            #Do the actual AUTH with FB, there is a lot of logic behind this
            user = authenticateFB(fb_access_token, fb_user_id)
    except Exception as ex:
        user = None

    #Determine if user info was valid
    if user is not None:
        if user.is_active:
            login(request, user)
            data = {'result':True, 'username':user.username}
            logger.info(logonUtility.formatLogMessage(request, "FB Login Successful"))
        else:
            data = {'result':False}
            logger.info(logonUtility.formatLogMessage(request, "FB Login Failed, user is inactive"))
    else:
        data = {'result':False}
        logger.info(logonUtility.formatLogMessage(request, "FB Login Failed"))

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


#PRIVATE METHOD
#Instead of making a new AUTH-BACKEND, lets just imitate it here
def authenticateFB(fb_access_token, fb_user_id):
    #Call up FB to see if the access_token is legit
    try:
        #fbURL = 'https://graph.facebook.com/app?access_token=%s' % (fb_access_token)
        #fbURL = 'https://graph.facebook.com/me?access_token=%s' % (fb_access_token)
        #response = urllib.request.urlopen(fbURL)

        #Build the batch request
        batchRequest = '[{"method":"GET","relative_url":"me"}, {"method":"GET","relative_url":"app"}]'
        batchRequestEncoded = urllib.parse.quote_plus(batchRequest)
        dataParams = 'access_token=%s&batch=%s' % (fb_access_token, batchRequestEncoded)
        dataParamsEncoded = dataParams.encode()
        fbURL = 'https://graph.facebook.com/'

        #Send a POST, and read the results
        response = urllib.request.urlopen(fbURL, dataParamsEncoded)
        data = response.read()
        data = json.loads(data.decode("utf-8"))

        #Extract the details
        userInfo = data[0]['body']
        userJson = json.loads(userInfo)
        appInfo = data[1]['body']
        appJson = json.loads(appInfo)
        dataFromFB = {"userData":userJson, "appData":appJson}

        #Make sure the access token was issued by our app for our user
        if ((settings.FB_APP_ID == dataFromFB['appData']['id']) and (fb_user_id == dataFromFB['userData']['id'])):
            #Extract the important FB personal info
            fb_email = dataFromFB['userData']['email']
            fb_first_name = dataFromFB['userData']['first_name']
            fb_last_name = dataFromFB['userData']['last_name']

            try:
                #see if the USER_FB object already exists
                dbUserFB = UserFB.objects.get(fb_user_id=fb_user_id)
            except Exception as ex1:
                #If the get() cant find the row, it will be caught here
                dbUserFB = None

            #see if we need to register the user (i.e. first time) or not
            if (dbUserFB is None):
                #We need an email address, None or Empty is bad
                if (not fb_email):
                    raise Exception('Email is missing')

                #First time, so we need to create the User and UserFB rows
                username = fb_email
                password = 'pass' + fb_user_id #For backdoor login, until RunAs is implemented
                dbUser = User.objects.create_user(username, fb_email, password, first_name=fb_first_name, last_name=fb_last_name)
                dbUserFB = UserFB(fb_user_id=fb_user_id, user=dbUser)
                dbUserFB.save()
            else:
                #Already exists, so lets just get the User from the DB
                dbUser = User.objects.get(pk=dbUserFB.user_id)

                #In case any of the users FB personal info has changed, lets save it away
                if (dbUser.email != fb_email or dbUser.first_name != fb_first_name or dbUser.last_name != fb_last_name):
                    dbUser.email = fb_email
                    dbUser.first_name = fb_first_name
                    dbUser.last_name = fb_last_name
                    dbUser.save()

            #Now we have a user, we need to simulate what typical authenticate(**) does
            #If we dont do this, login() will fail, and the user wont be in the session
            dbUser.backend = 'django.contrib.auth.backends.ModelBackend'
        else:
            #The dataFromFB didnt come from this appID or for this userID
            raise Exception('AppID or UserID didnt match')

    except Exception as ex2:
        #Something above broke, let the caller know
        dbUser = None
        raise

    #Return the AUTH user, based on the FB login
    return dbUser


def doLogout(request):
    logout(request)
    data = {'result':True}
    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
def doRegister(request):
    username = request.POST['username']
    password = request.POST['password']
    email = username
    user = User.objects.create_user(username, email, password)

    if user is not None:
        data = {'result':True}
    else:
        data = {'result':False}

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")


def setUserPassword(request):
    password1 = request.POST['password1']
    password2 = request.POST['password2']

    if (password1 == password2):
        request.user.set_password('new password')
        data = {'result':True}
    else:
        data = {'result':False}

    data = json.dumps(data)
    return HttpResponse(data, content_type="application/json")
