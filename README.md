Django 1.6 and Python 3 on OpenShift, with Facebook Login
=========================================================

This git repository helps you get up and running quickly w/ a Django 1.6 and
Python 3.3 installation on OpenShift. 

Further, it has implemented the Facebook SDK, JavaScript API Login.
Allowing you to use that as an example for implementing authentication.


Details
-------
	Deploy: Openshift Cloud
	Requires: Python 3.3, MySQL 5.5
	Facebook App: See settings.py (FB_APP_ID)
	Recommendations for local Dev: PyCharm, pip


More Details
------------
	HTML
	Bootstrap
	Font-Awesome
	AngularJS Controllers
	REST/JSON via Django Python API
	MySQL
